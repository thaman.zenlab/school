<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{


    protected $fillable = ['title','question','subject','type','price','image'];

    public function getAllStudent(){
        return $this->orderBy('id','ASC')->get();

    }

    public function getRules(){
        $rules = array(
            'title' => 'required|string',
            'question' => 'required|string',
            'subject' =>'required',
            'type' => 'required',
            'price' => 'required',
            'image' => 'sometimes|image|max:2048'
        );
        return $rules;
    }
}

