<?php

namespace App\Http\Controllers;

use App\Models\School;
/*use Faker\Provider\Image;*/
use Illuminate\Http\Request;
use File;
use Image;

class formController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school = new School();
       //dd($school);

        $data = $school->getAllStudent();
       // dd($data);


        return view('list_form')
            ->with('page_title',"Student List")
            ->with('title','Student Table')
            ->with('list_form',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $school = new School();

        $rules = $school->getRules();


        $request->validate($rules);

        $data = $request->all();


        if($request->has('image')){
           $file_name = "Image-".date('Ymdhis').rand(0,999).".".$request->image->getClientOriginalExtension();
           //dd($file_name);
           $path = public_path().'/upload';
           //dd($path);
            if(!File::exists($path)){
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if($success){

                $thumb_name = "Thumb-".$file_name;
                Image::make($path.'/'.$file_name)->resize(200,200, function ($constraint){

                    $constraint->aspectRatio();
                })->save($path.'/'.$thumb_name);

                $data['image']= $file_name;
            }else{
                $data['image'] = null;
            }
          //  dd($data);


        }

        //dd($school);
        $school->fill($data);

        $status = $school->save();
        // dd($status);
        return redirect(route('school.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $school = new School();
        dd($school);

        return view('news_detail');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $school = new School();
       //dd($school);

        $school = $school->find($id);


        if(!$school){
           return  redirect(route('school.index'));
       }

       return view('form')
           ->with('school_data', $school);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $school = new School();
        $school = $school->find($id);

        $rules = $school->getRules();

        $request->validate($rules);
        $data = $request->all();


        if($request->has('image')){
            $file_name = "Image-".date('Ymdhis').rand(0,999).".".$request->image->getClientOriginalExtension();
            //dd($file_name);
            $path = public_path().'/upload';
            //dd($path);
            if(!File::exists($path)){
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if($success){

                $thumb_name = "Thumb-".$file_name;
                Image::make($path.'/'.$file_name)->resize(200,200, function ($constraint){

                    $constraint->aspectRatio();
                })->save($path.'/'.$thumb_name);

                $data['image']= $file_name;
            }else{
                $data['image'] = null;
            }
            //  dd($data);


        }

        //dd($school);
        $school->fill($data);

        $status = $school->save();
        // dd($status);
        return redirect(route('school.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = new School();
        //dd($school);
        $school = $school->find($id);

        if($school){
            $school->delete();
        }
        return redirect(route('school.index'));
    }
}
