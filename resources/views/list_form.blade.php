@extends('layouts.master')
@section('title','Student List')


@section('content')
<div class="col-12">
    <h4 class="text-center">{{$title}}</h4>
    @include('section.create')
    <hr>
    <table class="table">
        <thead class="thead-dark">
        <th>S.N</th>
        <th>Title</th>
        <th>Question</th>
        <th>Subject</th>
        <th>Type</th>
        <th>Price</th>
        <th>Image</th>
        <th>Action</th>
        <th>Download</th>

        </thead>
        <tbody>
        {{--  {{$list_form}}--}}
        @foreach($list_form as $key =>$list_info)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$list_info->title}}</td>
                <td>{{$list_info->question}}</td>
                <td>{{$list_info->subject}}</td>
                <td>{{$list_info->type}}</td>
                <td>{{$list_info->price}}</td>
                <td>
                    <img src="{{ asset('upload/'.$list_info->image) }}" alt="" style="max-width: 100px" class="img img-responsive img-thumbnail">
                </td>
                <td>
                    <div class="form-group row">
                        <a href="{{route('school.edit',$list_info->id)}}"><button class=" btn btn-success btn-sm">Edit</button></a>

                        <form onsubmit="return confirm('Are you sure you want to delete this student ?')" action="{{route('school.destroy',$list_info->id)}}" method="post">
                            @csrf
                            @method('delete')
                            <button class=" btn btn-danger btn-sm">Delete</button>

                        </form>
                        <form action=" {{route('school.show',$list_info->id)}}" method="get">
                            @csrf
                            @method('show')
                            <button class=" btn btn-secondary btn-sm">View</button>

                        </form>
                    </div>
                </td>
                <td>Download</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@endsection
