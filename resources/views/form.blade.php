@extends('layouts.master')
@section('content')

    <div class="col-12">
        <h4 class="text-center">Question {{ isset($school_data) ? 'Update' : "Add" }}</h4>
        <hr>

        @if(isset($school_data))
            <form action="{{ route('school.update',$school_data->id) }}" method="post" enctype="multipart/form-data"
                  class="form-">
                @method('patch')
                @else
                    <form action="{{ route('school.store') }}" method="post" enctype="multipart/form-data"
                          class="form-">
                        @endif
                        @csrf

                        <div class="form-group row">
                            <label for="" class="col-sm-3">Title:</label>
                            <div class="col-sm-9">
                                <input type="text" name="title" value="{{@$school_data->title }}" id="title"
                                       class="form-control form-control-sm">
                                @error('title')
                                <span class="alert-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-3">Your Question:</label>
                            <div class="col-9">
                                <textarea name="question" id="question" rows="5" class="form-control form-control-sm"
                                          style="resize: none;">{{@$school_data->question}}</textarea>
                                @error('question')
                                <span class="alert-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-3">Subject:</label>
                            <div class="col-sm-9">
                                <select name="subject"  id="subject" class="form-control form-control-sm">
                                    <option value="BCA">BCA</option>
                                    <option value="BIT">BIT</option>
                                    <option value="CSIT">CSIT</option>
                                    <option value="BBM">BBM</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-3">Type:</label>
                            <div class="col-sm-9">
                                <select name="type" id="type" class="form-control form-control-sm">
                                    <option value="short">Short</option>
                                    <option value="long">Long</option>
                                    <option value="verylong">Very Long</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-3">Price:</label>
                            <div class="col-9">
                                <select name="price" id="price" class="form-control form-control-sm">
                                    <option value="500">500</option>
                                    <option value="200">200</option>
                                    <option value="400">400</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-3">Image:</label>
                            <div class="col-6">
                                <input type="file" value="" onchange="readURL(this)" name="image" accept="*/*">
                            </div>
                            <div class="col-3">
                                <img src="{{ @asset('upload/'.$school_data->image) }}" alt="" id="thumb" class="img img-thumbnail">
                            </div>

                        </div>




                        <div class="form-group row">
                            <label for="" class="col-3"></label>
                            <div class="col-9">
                                <button class="btn btn-warning" type="reset">
                                    Reset
                                </button>
                                <button class="btn btn-primary" type="submit">
                                    Submit
                                </button>
                            </div>
                        </div>

                    </form>
    </div>
@endsection



@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#thumb').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>



@endsection
