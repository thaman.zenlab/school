<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $array = array(
        array(
            'type' => 'Long',
            'price' => '500',
        ),
        array(
            'type' => 'Long',
            'price' => '400',
        ),
        array(
            'type' => 'Short',
            'price' => '200',
        ),
        array(
            'type' => 'Very Long',
            'price' => '100',
        )

    );

    DB::table('tags')->insert($array);


    }
}
