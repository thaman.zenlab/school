<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::table('schools', function (Blueprint $table){
            $table->unsignedBigInteger('tags_id')->nullable()->after('title');
            $table->foreign('tags_id')->references('id')->on('tags')->onDelete('SET NULL')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('schools', function (Blueprint $table) {
            $table->dropColumn('tags_id');
        });
    }
}
